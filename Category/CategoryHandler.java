package Category;
public class CategoryHandler {

    private CategoryOperations categoryOperations;
    public CategoryHandler()
    {
        this.categoryOperations = new CategoryOperations();
    }

    public boolean validateCategoryData(String name, double budget)
    {
        return !name.isEmpty() && budget > 0;
    }

    public boolean handleCreateCategory(String name, double budget)
    {
        if(validateCategoryData(name, budget))
        {
            Category category = new Category(name, budget);

            categoryOperations.createCategory(category);
            System.out.println("Category created successfully.");
            return true;
        }

        else{
            System.out.println("Invalid category data. Please try again.");
            return false;
        }
    }

    public boolean handleEditCategory(int categoryID,String name, double budget)
    {
        if(validateCategoryData(name, budget))
        {
            Category category = new Category(categoryID,name, budget);
            
            categoryOperations.editCategory(category);
            System.out.println("Category edited successfully.");
            return true;
        }

        else{
            System.out.println("Invalid category data. Please try again.");
            return false;
        }
    }

    public boolean handleDeleteCategory(int categoryID)
    {
        if(categoryID >=1)
        {

        
        categoryOperations.deleteCategory(categoryID);
        System.out.println("Category with Category ID: " + categoryID + " deleted successfully. ");
        return true;
        }

        else
        {
            System.out.println("Categry could not be found: ");
            return false;
        }

    }
    
}
