package Category;
public class CategoryController {
    
    private CategoryHandler categoryHandler;

    public CategoryController()
    {
        this.categoryHandler = new CategoryHandler();
    }

    public boolean createCategory(String name, double budget)
    {
        return categoryHandler.handleCreateCategory(name,budget);

        
    }

    public boolean editCategory(int categoryID, String name, double budget)
    {
        return categoryHandler.handleEditCategory(categoryID, name, budget);
    }

    public boolean deleteCategory(int categoryID)
    {
        return categoryHandler.handleDeleteCategory(categoryID);
    }
}
