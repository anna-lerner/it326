package Category;

public class Category {
    private int categoryID;
    private int expenseID;
    private String name;
    private double budget;

    public Category(String name, double budget) {
        this.categoryID = generateCategoryID();
        this.expenseID = 0;
        this.name = name;
        this.budget = budget;
    }

    public Category(int categoryID, String name, double budget) {
        this.categoryID = categoryID;
        this.name = name;
        this.budget = budget;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID)
    {
        this.categoryID = categoryID;
    }

    public int getExpenseID() {
        return expenseID;
    }

    public void setExpenseID(int expenseID) {
        this.expenseID = expenseID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    private int generateCategoryID() {
        return (int) (Math.random() * 1000);
    }
}