package Category;

import Database.SQLDatabase;

public class CategoryOperations {

    private SQLDatabase db;

    public CategoryOperations() {
        this.db = new SQLDatabase();
    }

    public void createCategory(Category category) {
        saveCategoryToSQLDatabase(category);
    }

    public void editCategory(Category category) {
        db.editCategory(category);
    }

    public void deleteCategory(int categoryID) {
        deleteCategoryFromSQLDatabase(categoryID);
    }

    private void saveCategoryToSQLDatabase(Category category) {
        db.createCategory(category);
    }

    private void deleteCategoryFromSQLDatabase(int categoryID) {
        db.deleteCategory(categoryID);
    }

}
