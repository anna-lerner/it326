package Database;

public abstract class Database {
    public abstract void storeUser(int userid, String username, String password, String email, String name,
            String income);

    public abstract void retrieveExpenses(String mode, int userId, String expenseName, double minPrice, double maxPrice,
            int categoryId);
}
