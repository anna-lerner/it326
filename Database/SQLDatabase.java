package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Category.Category;
import Expense.Expense;
import Investment.Investment;

public class SQLDatabase extends Database {

    // Database connection details
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/it326";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "r0otp@s5word!";

    @Override
    public void retrieveExpenses(String mode, int userId, String expenseName, double minPrice, double maxPrice,
            int categoryId) {
        if (mode.equals("date")) {
            String query = "SELECT name, amount, date FROM expenses WHERE userid = ? ORDER BY date";

            try (
                    // Establishing a connection to the database
                    Connection connection = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                    // Creating a prepared statement with the query
                    PreparedStatement preparedStatement = connection.prepareStatement(query);) {
                // Setting the userid parameter in the prepared statement
                preparedStatement.setInt(1, userId);

                // Executing the query
                ResultSet resultSet = preparedStatement.executeQuery();

                // Displaying the results
                while (resultSet.next()) {
                    String name = resultSet.getString("name");
                    String amount = resultSet.getString("amount");
                    String date = resultSet.getString("date");
                    System.out.println("Name: " + name + ", Amount: " + amount + ", Date: " + date);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (mode.equals("price")) {
            // Nested if for filtering by price
            String query = "SELECT name, amount, date FROM expenses WHERE userid = ? AND amount BETWEEN ? AND ? ORDER BY date";

            try (
                    // Establishing a connection to the database
                    Connection connection = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                    // Creating a prepared statement with the query
                    PreparedStatement preparedStatement = connection.prepareStatement(query);) {
                // Setting the userid, minPrice, and maxPrice parameters in the prepared
                // statement
                preparedStatement.setInt(1, userId);
                preparedStatement.setDouble(2, minPrice);
                preparedStatement.setDouble(3, maxPrice);

                // Executing the query
                ResultSet resultSet = preparedStatement.executeQuery();

                // Displaying the results
                while (resultSet.next()) {
                    String name = resultSet.getString("name");
                    String amount = resultSet.getString("amount");
                    String date = resultSet.getString("date");
                    System.out.println("Name: " + name + ", Amount: " + amount + ", Date: " + date);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (mode.equals("category")) {
            // Nested if for filtering by category
            String query = "SELECT name, amount, date FROM expenses WHERE userid = ? AND categoryId = ? ORDER BY date";

            try (
                    // Establishing a connection to the database
                    Connection connection = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                    // Creating a prepared statement with the query
                    PreparedStatement preparedStatement = connection.prepareStatement(query);) {
                // Setting the userid and categoryId parameters in the prepared statement
                preparedStatement.setInt(1, userId);
                preparedStatement.setInt(2, categoryId);

                // Executing the query
                ResultSet resultSet = preparedStatement.executeQuery();

                // Displaying the results
                while (resultSet.next()) {
                    String name = resultSet.getString("name");
                    String amount = resultSet.getString("amount");
                    String date = resultSet.getString("date");
                    System.out.println("Name: " + name + ", Amount: " + amount + ", Date: " + date);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (mode.equals("name")) {
            String query = "SELECT name, amount, date FROM expenses WHERE userid = ? AND name = ?";

            try (
                    // Establishing a connection to the database
                    Connection connection = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                    // Creating a prepared statement with the query
                    PreparedStatement preparedStatement = connection.prepareStatement(query);) {
                // Setting userid and expenseName parameters in the prepared statement
                preparedStatement.setInt(1, userId);
                preparedStatement.setString(2, expenseName);

                // Executing the query
                ResultSet resultSet = preparedStatement.executeQuery();

                // Displaying the results
                while (resultSet.next()) {
                    String name = resultSet.getString("name");
                    String amount = resultSet.getString("amount");
                    String date = resultSet.getString("date");
                    System.out.println("Name: " + name + ", Amount: " + amount + ", Date: " + date);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return;
    }

    // Method to store username and password in the database
    public void storeUser(int userid, String username, String password, String email, String name, String income) {
        try { // Database connection stuff
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
            // Build query
            String insertQuery = "INSERT INTO accounts (userid, users, passwords, email, name, income) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = con.prepareStatement(insertQuery);

            // Setting parameter values
            preparedStatement.setString(1, String.valueOf(userid));
            preparedStatement.setString(2, username);
            preparedStatement.setString(3, password);
            preparedStatement.setString(4, email);
            preparedStatement.setString(5, name);
            preparedStatement.setString(6, income);

            // Executing the INSERT statement
            int rowsAffected = preparedStatement.executeUpdate();
            if (rowsAffected > 0) {
                System.out.println("Data inserted successfully.");
            } else {
                System.out.println("Failed to insert data.");
            }
            preparedStatement.close();
            con.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createCategory(Category category) {
        String query = "INSERT INTO Categories (CategoryID, Name, Budget) VALUES (?, ?, ?)";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
            PreparedStatement preparedStatement = con.prepareStatement(query);
            preparedStatement.setInt(1, category.getCategoryID());
            preparedStatement.setString(2, category.getName());
            preparedStatement.setDouble(3, category.getBudget());
            preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void editCategory(Category category) {

        String query = "UPDATE categories SET name = ?, budget = ? WHERE CategoryID = ?";

        try {
            System.out.println("Done   ");
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
            PreparedStatement preparedStatement = con.prepareStatement(query);
            preparedStatement.setString(1, category.getName());
            preparedStatement.setDouble(2, category.getBudget());
            preparedStatement.setInt(3, category.getCategoryID());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void deleteCategory(int categoryID) {
        String query = "DELETE FROM Categories WHERE CategoryID = ?";

        try (Connection connection = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setInt(1, categoryID);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean verifyUserCredentials(String username, String password) {
        String sql = "SELECT COUNT(*) FROM accounts WHERE users = ? AND passwords = ?";
        try (Connection conn = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (SQLException e) {
            System.err.println("Database error during login: " + e.getMessage());
        }
        return false;
    }

    public double calculateTotalExpenses(int userID) {
        double totalExpenses = 0.0;
        String query = "SELECT SUM(amount) AS total_expenses FROM expenses WHERE userid = ?";

        try (Connection connection = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            // Set the userID parameter in the prepared statement
            preparedStatement.setInt(1, userID);

            // Execute the query
            ResultSet resultSet = preparedStatement.executeQuery();

            // Retrieve the total expenses from the result set
            if (resultSet.next()) {
                totalExpenses = resultSet.getDouble("total_expenses");
        
            }

            System.out.println("Total expenses:" + totalExpenses);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return totalExpenses;
    }

    // Method to store expense in the database
    public void storeExpense(Expense expense) {
        String sql = "INSERT INTO expenses (expenseID, name, amount, date, userid, CategoryID) VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection conn = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            System.out.println("ExpenseID " +expense.getExpenseID());
            System.out.println("Expense name " +expense.getName());
            System.out.println("Expense amount " +expense.getAmount());
            System.out.println("Expense Date " +expense.getDate());
            System.out.println("User ID " +expense.getUser().getUserid());
            System.out.println("Category ID " +expense.getCategory().getCategoryID());
            pstmt.setInt(1, expense.getExpenseID());
            pstmt.setString(2, expense.getName());
            pstmt.setDouble(3, expense.getAmount());
            pstmt.setString(4, expense.getDate());
            pstmt.setInt(5, expense.getUser().getUserid());
            pstmt.setInt(6, expense.getCategory().getCategoryID());
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Method to delete an expense from the database
    public void deleteExpense(int expenseID) {
        String sql = "DELETE FROM expenses WHERE expenseID = ?";
        try (Connection conn = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, expenseID);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Method to update an existing expense in the database
    public void updateExpense(Expense expense) {
        String sql = "UPDATE expenses SET name = ?, amount = ?, date = ?, categoryID = ?, userid = ? WHERE expenseID = ?";
        try (Connection conn = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, expense.getName());
            pstmt.setDouble(2, expense.getAmount());
            pstmt.setString(3, expense.getDate());
            pstmt.setInt(4, expense.getCategory().getCategoryID());
            pstmt.setInt(5, expense.getUser().getUserid());
            pstmt.setInt(6, expense.getExpenseID());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Method to create an investment in the database
    public int createInvestment(Investment investment) {
        String sql = "INSERT INTO investments (investmentID, name, investmentAmount) VALUES (?, ?, ?)";
        try (Connection conn = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, investment.getInvestmentID());
            pstmt.setString(2, investment.getName());
            pstmt.setDouble(3, investment.getInvestmentAmount());
            pstmt.executeUpdate();

            // Retrieve the auto-generated investment ID
            ResultSet generatedKeys = pstmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1; // Return -1 if insertion fails
    }

    // Method to delete an investment from the database
    public void deleteInvestment(int investmentID) {
        String sql = "DELETE FROM investments WHERE investmentID = ?";
        try (Connection conn = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, investmentID);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Method to edit an existing investment in the database
    public void editInvestment(Investment investment) {
        String sql = "UPDATE investments SET name = ?, investmentAmount = ? WHERE investmentID = ?";
        try (Connection conn = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, investment.getName());
            pstmt.setDouble(2, investment.getInvestmentAmount());
            pstmt.setInt(3, investment.getInvestmentID());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getAllExpenses(int userID)
    {
        String query = "SELECT * FROM expenses WHERE userid = ?";

        try (
            // Establishing a connection to the database
            Connection connection = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
            // Creating a prepared statement with the query
            PreparedStatement preparedStatement = connection.prepareStatement(query);
        ) {
            // Setting the user ID parameter in the prepared statement
            preparedStatement.setInt(1, userID);

            // Executing the query
            ResultSet resultSet = preparedStatement.executeQuery();

            // Printing the results
            while (resultSet.next()) {
                int expenseId = resultSet.getInt("expenseId");
                String name = resultSet.getString("name");
                double amount = resultSet.getDouble("amount");
                String date = resultSet.getString("date");
                int categoryId = resultSet.getInt("categoryId"); // Assuming categoryId exists in the expenses table
                System.out.println("Expense ID: " + expenseId + ", Name: " + name + ", Amount: " + amount + ", Date: " + date + ", Category ID: " + categoryId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
        
}


