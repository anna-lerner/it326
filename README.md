# IT326

Here is the repo for our XP Application. 

- Create a Gitlab account
- Send me your Gitlab account email. I will send you an invitation to access the project with a developer role.
- If you have not used SSH before, you will need to set it up on your machine and add your public key to your Gitlab account. If you do not do  this it will give you an error when you try to clone the project.
- On the main page of the repo, select the blue button that says "code"
    - From the drop-down, select "clone with SSH" by clicking the copy button
- Open VScode (or whatever IDE you prefer). On the home page there will be a link that says "clone git repository". Select that, and paste in the SSH link you copied. 

The project should now be open in your IDE. Before you start developing, you'll want to create your own development branch. This basically means you'll be developing in your own environment, and can make changes to your code without affecting the main repo. The idea is to keep the main repo functional. The command to create your own branch is: git checkout -b <yournamehere>. To save your changes, you will perform "pushes" to your branch. If you're using VScode, the Version Control button on the left makes this process easy. You can also do it through commands via the terminal.

Let me know if you have any questions.
