package Notification;

import Notification.EmailNotification;
import User.User;

import java.util.*; 
import javax.mail.*; 
import javax.mail.internet.*; 

public class EmailNotification implements Notification
{

    String subject = "XP Application";
    String emailRecipient;
    Boolean isPerformanceGood;
    User user;

    String[] messageTemplates = { "Your portfolio has improved today!", "Your investments did not do so well today." };

    public EmailNotification(User user, Boolean isPerformanceGood, String recipient){
        this.user = user;
        this.emailRecipient = recipient;
        this.isPerformanceGood = isPerformanceGood;
    }

    public boolean sendNotification()
    {
        String senderEmail = "grayson.newcomer@gmail.com";
        String senderPassword = "rsxd dvgt tnfa rktt";
        String msg = "";

        if(isPerformanceGood){
            msg = messageTemplates[0];
        }
        else{
            msg = messageTemplates[1];
        }

        // SMTP server settings
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.ssl.protocols", "TLSv1.2");

        // Get the Session object
        Session session = Session.getInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderEmail, senderPassword);
            }
        });

        try {
            // Create a MimeMessage object
            Message message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(senderEmail));

            // Set To: header field of the header.
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailRecipient));

            // Set Subject: header field
            message.setSubject(subject);

            // Now set the actual message
            message.setText(msg);

            // Send message
            Transport.send(message);
            System.out.println("Email sent successfully.");
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
      return true;
    }

}
