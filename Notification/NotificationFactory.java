package Notification;

import User.User;

public class NotificationFactory {
    
    public static Notification getNotification(User user, Boolean isPerformanceGood, String type, String recipient){
        if(type.equals("email")){
            return new EmailNotification(user, isPerformanceGood, recipient);
        }
        else if(type.equals("sms")){
            return new SMSNotification(recipient);
        }
        return null;
    }

}
