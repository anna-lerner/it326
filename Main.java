import java.util.Scanner;

import Category.Category;
import Category.CategoryController;
import Expense.Expense;
import Expense.ExpenseController;
import Investment.InvestmentController;
import Investment.InvestmentHandler;

import java.util.Random;

import User.User;
import User.UserController;

public class Main {

    public static final String EXPENSES_BY_DATE_MODE = "date";
    public static final String EXPENSES_BY_NAME_MODE = "name";
    public static final String EXPENSES_BY_PRICE_MODE = "price";
    public static final String EXPENSES_BY_CATEGORY_MODE = "category";

    public static void main(String[] args) {
        String option;
        Scanner input = new Scanner(System.in);
        Boolean shouldSendNotification = false;
        Boolean isPerformanceGood = false;
        Boolean isLoggedIn = false;

        // Initial Welcome Menu
        System.out.println("Hello! Welcome to XP!\n--------------------");
        do {
            User user = new User();
            User loggedUser = user;
            

            // Options to login (1) or create account (2)
            System.out.print(
                    "Enter '1' to login\nEnter '2' to create account\nEnter 'X' to exit\nOption: ");
            option = input.next();
            if (option.equals("1") || option.equals("3")) {
                if (option.equals("1")) {
                    System.out.print("Please enter username: ");
                    String username = input.next();
                    user.setUsername(username);
                    System.out.print("Please enter password: ");
                    String password = input.next();
                    user.setPassword(password);
                    isLoggedIn = UserController.loginUser(username, password);
                    
                    
                    if (isLoggedIn) {
                        loggedUser = UserController.loginUserAndGetInfo(username,password);
                        System.out.println("userid: " + loggedUser.getUserid());
                        System.out.println("Login successful.");
                         // Assuming this method sets the logged in user
                    } else {
                        System.out.println("Login failed. Please check your username and password.");
                    }
                }

                // All other uses cases
                while (!option.equals("x") && isLoggedIn) {
                    isLoggedIn = true;
                    /* Menu Loop */
                    System.out.println("--------------------");
                    System.out.println("1. Create Expense");
                    System.out.println("2. Edit Expense");
                    System.out.println("3. Delete Expense");
                    System.out.println("4. Search Expenses");
                    System.out.println("5. Create Category");
                    System.out.println("6. Edit Category");
                    System.out.println("7. Delete Category");
                    System.out.println("8. Create Investment");
                    System.out.println("9. Edit Investment");
                    System.out.println("10. Delete Investment");
                    System.out.println("11. Fetch Stock Price");
                    System.out.println("12. Calculate total expenses");
                    System.out.println("13. Receive transaction history");
                    System.out.println("14. Logout");
                    System.out.println("--------------------");
                    System.out.println("Choose a number for you option.");
                    System.out.print("Please select an option: ");

                    option = input.next();

                    // Create Expense
                    if (option.equals("1")) {
                        System.out.println("Enter Expense ID: ");
                        int expenseID = input.nextInt();
                        System.out.println("Enter Expense Name: ");
                        String name = input.next();
                        System.out.println("Enter Amount: ");
                        double amount = input.nextDouble();
                        System.out.println("Enter Date (YYYY-MM-DD): ");
                        String date = input.next();
                        System.out.println("Enter the Category Name ");
                        String categoryName = input.next();
                        System.out.println("Enter CategoryID for Expense: ");
                        int categoryID = input.nextInt();

                        Category category = new Category(categoryName, 0);
                        category.setCategoryID(categoryID);
                        Expense newExpense = new Expense(expenseID, name, amount, date, category, loggedUser);
                        System.out.println("CategoryID: " + category.getCategoryID());
                        ExpenseController ec = new ExpenseController();
                        if (ec.createExpense(newExpense)) {
                            System.out.println("Expense created successfully.");
                        } else {
                            System.out.println("Failed to create expense.");
                        }
                    }

                    // Edit Expense
                    else if (option.equals("2")) {
                        System.out.println("Enter Expense ID to edit: ");
                        int expenseID = input.nextInt();
                        System.out.println("Enter new Expense Name: ");
                        String name = input.next();
                        System.out.println("Enter new Amount: ");
                        double amount = input.nextDouble();
                        System.out.println("Enter new Date (YYYY-MM-DD): ");
                        String date = input.next();
                        System.out.println("Enter Category for Expense: ");
                        String categoryName = input.next();
                        System.out.println("Enter the CategoryID ");
                        int categoryID = input.nextInt();
                        Category category = new Category(categoryName, 0);
                        category.setCategoryID(categoryID);
                        Expense updatedExpense = new Expense(expenseID, name, amount, date, category,
                                loggedUser);
                        ExpenseController ec = new ExpenseController();
                        if (ec.editExpense(updatedExpense)) {
                            System.out.println("Expense updated successfully.");
                        } else {
                            System.out.println("Failed to update expense.");
                        }
                    }

                    // Delete Expense
                    else if (option.equals("3")) {
                        System.out.println("Enter Expense ID to delete: ");
                        int expenseID = input.nextInt();
                        ExpenseController ec = new ExpenseController();
                        if (ec.deleteExpense(expenseID)) {
                            System.out.println("Expense deleted successfully.");
                        } else {
                            System.out.println("Failed to delete expense.");
                        }
                    }

                    // Search expenses option
                    else if (option.equals("4")) {
                        System.out.println("----------------------");
                        System.out.println("1. Display by date");
                        System.out.println("2. Search by name");
                        System.out.println("3. Filter by price");
                        System.out.println("4. Filter by category");
                        System.out.println("----------------------");
                        System.out.print("Please select an option: ");
                        option = input.next();
                        if (option.equals("1")) {
                            ExpenseController.displayExpenses(EXPENSES_BY_DATE_MODE, loggedUser.getUserid(), "", 0.0, 0.0, 0);
                        } else if (option.equals("2")) {
                            System.out.print("Enter expense name: ");
                            option = input.next();
                            ExpenseController.displayExpenses(EXPENSES_BY_NAME_MODE, loggedUser.getUserid(), option, 0.0, 0.0,
                                    0);
                        } else if (option.equals("3")) {
                            System.out.print("Enter minimum price: ");
                            double minPrice = input.nextDouble();
                            System.out.print("Enter maximum price: ");
                            double maxPrice = input.nextDouble();
                            ExpenseController.displayExpenses(EXPENSES_BY_PRICE_MODE, loggedUser.getUserid(), "", minPrice,
                                    maxPrice, 0);
                        } else if (option.equals("4")) {
                            System.out.print("Enter category id: ");
                            int categoryId = input.nextInt();
                            ExpenseController.displayExpenses(EXPENSES_BY_CATEGORY_MODE, loggedUser.getUserid(), "", 0.0, 0.0,
                                    categoryId);
                        }
                    }

                    else if (option.equals("5")) {
                        System.out.println("Enter a category name: ");
                        String categoryName = input.next();
                        System.out.println("Enter category budget: ");
                        double budget = input.nextDouble();
                        CategoryController c = new CategoryController();
                        c.createCategory(categoryName, budget);

                    }

                    else if (option.equals("6")) {
                        System.out.println("Enter category ID to edit: ");
                        int categoryID = input.nextInt();
                        System.out.println("Enter new category name: ");
                        String name = input.next();
                        System.out.println("Enter new category budget: ");
                        double budget = input.nextDouble();
                        CategoryController c = new CategoryController();
                        c.editCategory(categoryID, name, budget);

                    }

                    else if (option.equals("7")) {
                        System.out.println("Enter a category ID to delete");
                        int categoryID = input.nextInt();
                        CategoryController c = new CategoryController();
                        c.deleteCategory(categoryID);

                    }

                    // Create investment option
                    else if (option.equals("8")) {
                        System.out.print("Enter investment name: ");
                        String name = input.next();
                        System.out.print("Enter investment amount: ");
                        double amount = input.nextDouble();
                        InvestmentController.createInvestment(user.getUsername(), user.getPassword(), name, amount);
                    }

                    // Edit investment option
                    else if (option.equals("9")) {
                        System.out.print("Enter investment ID to edit: ");
                        int investmentID = input.nextInt();
                        System.out.print("Enter new investment name: ");
                        String name = input.next();
                        System.out.print("Enter new investment amount: ");
                        double amount = input.nextDouble();
                        InvestmentController.editInvestment(investmentID, name,
                                amount);
                    }

                    // Delete investment option
                    else if (option.equals("10")) {
                        System.out.print("Enter investment ID to delete: ");
                        int investmentID = input.nextInt();
                        InvestmentController.deleteInvestment(investmentID);
                        System.out.println("Investment deleted successfully!");
                    }

                    // Get stock price option
                    else if (option.equals("11")) {
                        System.out.print("Enter stock symbol: ");
                        String symbol = input.next();
                        double stockPrice = InvestmentHandler.getStockPrice(symbol);
                        System.out.println("Stock Price for " + symbol + ": " + stockPrice);
                    }


                else if(option.equals("12"))
                {
                    System.out.println("Calculating total expenses......");
                    System.out.println("UserID: " +loggedUser.getUserid());
                    
                    ExpenseController ex = new ExpenseController();
                    ex.calculateTotalExpenses(loggedUser.getUserid());
                    
                }

                if(option.equals("13"))
                {
                    System.out.println("Retrieving history......");
                    //ExpenseController.getAllExpenses(user.getUserid());
                    
                }

                    // Logout option
                    else if (option.equals("14")) {
                        System.out.println("Logging out and returning to menu...\n");
                        UserController.logout(user);
                        isLoggedIn = false;
                        break;
                    }
                    if (shouldSendNotification) {
                        // Notification option
                        UserController.sendNotification(user, isPerformanceGood, "email");

                    }
                }
            }

            // Create account block
            else if (option.equals("2")) {
                // Generate random userid
                Random random = new Random();
                int randomNum = random.nextInt(899) + 101;
                // Get username and password
                System.out.print("Please enter username: ");
                String username = input.next();
                System.out.print("Please enter password: ");
                String password = input.next();
                System.out.print("Please enter email: ");
                String email = input.next();
                System.out.print("Please enter name as one word (Underlined): ");
                String name = input.next();
                System.out.print("Please enter income: ");
                String income = input.next();
                // Insert user into database
                user.setUserid(randomNum);
                user.setUsername(username);
                user.setPassword(password);
                user.setEmail(email);
                user.setName(name);
                user.setIncome(Double.parseDouble(income));
                UserController.createProfile(randomNum, username, password, email, name, Double.parseDouble(income));
                isLoggedIn = true;
            }

            else {
                System.out.println("Invalid option. Please enter number between '1', '2', or 'X' to exit.");
                continue;
            }

        } while (!option.equals("x"));

        input.close();
    }
}
