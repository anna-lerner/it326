package test;
import static org.junit.Assert.*;

import org.junit.Test;
import Category.CategoryHandler;

public class CreateCategoryTest {

    @Test
    public void testCreateCategory_Success() {
        
        CategoryHandler categoryHandler = new CategoryHandler();
        String categoryName = "Groceries";
        double budget = 200.0;

        
        boolean result = categoryHandler.handleCreateCategory(categoryName, budget);

        assertTrue(result);
        
    }

    @Test
    public void testCreateCategory_InvalidBudget() {
        
        CategoryHandler categoryHandler = new CategoryHandler();
        String categoryName = "Entertainment";
        double budget = -100.0; // Invalid negative budget

        
        boolean result = categoryHandler.handleCreateCategory(categoryName, budget);

        
        assertFalse(result);
    }

    @Test
    public void testCreateCategory_EmptyName() {
        CategoryHandler categoryHandler = new CategoryHandler();
        String categoryName = ""; // Empty name
        double budget = 300.0;
        boolean result = categoryHandler.handleCreateCategory(categoryName, budget);
    
        assertFalse(result);
        
    }

    @Test
    public void testCreateCategory_NullName() {
        
        CategoryHandler categoryHandler = new CategoryHandler();
        String categoryName = null; // Null name
        double budget = 400.0;

        boolean result = categoryHandler.handleCreateCategory(categoryName, budget);

        assertFalse(result);
        
    }
}