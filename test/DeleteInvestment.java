import static org.junit.Assert.*;
import org.junit.Test;

public class DeleteInvestmentTest {

    @Test
    public void testDeleteInvestment() {
        // Delete an existing investment
        String username = "testUser";
        String password = "testPassword";
        int investmentID = 123; // ID of the investment to delete

        // Perform the delete operation
        boolean deleted = InvestmentController.deleteInvestment(username, password, investmentID);

        // Assert that the investment was successfully deleted
        assertTrue(deleted);
    }
}
