package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import User.User;
import User.UserController;

public class LogoutTest
{

    public static void main(String[] args)
    {
        testLogoutHappyCase();
        testBadLogout();
    }

    // Happy case to log out user
    @Test
    public static void testLogoutHappyCase()
    {
        User user = new User(432, "username", "password", "email.com", "name", 3848.43);
        User expectedUser = new User(0, null, null, null, null, 0);
        user = UserController.logout(user);
        assertEquals(user.getUserid(), expectedUser.getUserid());
        assertEquals(user.getUsername(), expectedUser.getUsername());
        assertEquals(user.getPassword(), expectedUser.getPassword());
        assertEquals(user.getEmail(), expectedUser.getEmail());
        assertEquals(user.getName(), expectedUser.getName());
        assertEquals(user.getIncome(), expectedUser.getIncome(), 0);

    }

    //Test logout with a bad parameter which should fail the whole process
    @Test
    public static void testBadLogout()
    {
        User user = new User(0, "user", "password", "email", "name", 0);
        user = UserController.logout(user);
        User expectedUser = new User(0, null, null, null, null, 0);
        assertEquals(user.getUserid(), expectedUser.getUserid());
        assertEquals(user.getUsername(), expectedUser.getUsername());
        assertEquals(user.getPassword(), expectedUser.getPassword());
        assertEquals(user.getEmail(), expectedUser.getEmail());
        assertEquals(user.getName(), expectedUser.getName());
        assertEquals(user.getIncome(), expectedUser.getIncome(), 0);

    }

}
