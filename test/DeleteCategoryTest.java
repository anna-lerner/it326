package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import Category.CategoryHandler;

public class DeleteCategoryTest {
    @Test
    public void testDeleteCategory_Success() {
        // Arrange
        CategoryHandler categoryHandler = new CategoryHandler();
        int categoryId = 123; // Existing category ID

        // Act
        boolean result = categoryHandler.handleDeleteCategory(categoryId);

        // Assert
        assertTrue(result);
        // Optionally, you can also verify if the category was deleted successfully from the database
    }

    @Test
    public void testDeleteCategory_InvalidCategoryID() {
        // Arrange
        CategoryHandler categoryHandler = new CategoryHandler();
        int categoryId = -1; // Invalid negative category ID

        // Act
        boolean result = categoryHandler.handleDeleteCategory(categoryId);

        // Assert
        assertFalse(result);
        // Optionally, you can verify if an appropriate error message is returned
    }

    @Test
    public void testDeleteCategory_NonExistentCategoryID() {
        // Arrange
        CategoryHandler categoryHandler = new CategoryHandler();
        int categoryId = 999; // Non-existent category ID

        // Act
        boolean result = categoryHandler.handleDeleteCategory(categoryId);

        // Assert
        assertFalse(result);
        // Optionally, you can verify if an appropriate error message is returned
    }
    
}