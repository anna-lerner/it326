package test;

import User.UserHandler;

import org.junit.Test;
import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CreateAccountTest
{
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/it326";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "r0otp@s5word!";
    public static void main(String[] args) {
        testBadCreateProfile();
        testWorkingCreateProfile();
    }
    

    /* Test Create Profile with bad information */
    @Test
    public static void testBadCreateProfile()
    {
        Boolean didWork = UserHandler.handleCreateProfile(0, "", "password", "email", "name", 0);
        assertEquals(false, didWork);
    }

    /*Add a user to the database and query the database to see if information is stored*/
    @Test static void testWorkingCreateProfile(){
        Boolean didWork = UserHandler.handleCreateProfile(9999, "username","password", "email.com", "name", 12345.67);
        assertEquals(true, didWork);
        String finalResult = "";
        String expectedResult = "UserID: 9999, Username: username, Password: password, Email: email.com, Name: name, Income: 12345.67";
        String query = "SELECT userid, users, passwords, email, name, income FROM accounts WHERE userid = ?";

            try (
                    // Establishing a connection to the database
                    Connection connection = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                    // Creating a prepared statement with the query
                    PreparedStatement preparedStatement = connection.prepareStatement(query);)
            {
                // Setting userid and expenseName parameters in the prepared statement
                preparedStatement.setInt(1, 9999);

                // Executing the query
                ResultSet resultSet = preparedStatement.executeQuery();

                // Displaying the results
                while (resultSet.next())
                {
                    String userid = resultSet.getString("userid");
                    String username = resultSet.getString("users");
                    String password = resultSet.getString("passwords");
                    String email = resultSet.getString("email");
                    String name = resultSet.getString("name");
                    String income = resultSet.getString("income");
                    finalResult = ("UserID: " + userid + ", Username: " + username + ", Password: " + password + ", Email: " + email + ", Name: " + name + ", Income: " + income);
                }
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }

            assertEquals(expectedResult, finalResult);


    }

    
}
