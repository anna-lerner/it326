import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CreateInvestmentTest {

    private String username;
    private String password;
    private String name;
    private double amount;

    @Before
    public void setUp() {
        // Initialize test data before each test case
        username = "testUser";
        password = "testPassword";
        name = "Test Investment";
        amount = 1000.0;
    }

    @Test
    public void testCreateInvestment() {
        // Create a new investment
        int investmentID = InvestmentController.createInvestment(username, password, name, amount);

        // Assert that investment ID is not -1 (indicating successful creation)
        assertNotEquals(-1, investmentID);
    }

    @After
    public void tearDown() {
        // Clean up resources after each test case if needed
        // For example, delete any test data created during testing
        // This method will be executed after each test case
    }
}
