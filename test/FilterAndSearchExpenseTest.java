package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import Expense.ExpenseHandler;

public class FilterAndSearchExpenseTest
{

    public static void main(String[] args)
    {
        //Happy cases that should work
        displayExpenseNameHappyCase();
        displayExpenseDateHappyCase();

        //Test cases that should fail
        try
        {
            testDisplayExpenseBadMode();
        }
        catch (AssertionError e)
        {
            System.out.println("Assertion failed: " + e);
        }

        try
        {
            testDisplayExpenseBadUserid();
        }
        catch (AssertionError e)
        {
            System.out.println("Assertion failed: " + e);
        }

        try
        {
            testDisplayExpenseBadName();
        }
        catch (AssertionError e)
        {
            System.out.println("Assertion failed: " + e);
        }
       
    }

    /* Testing Display Expense with bad mode parameter */
    @Test
    public static void testDisplayExpenseBadMode()
    {
        Boolean didWork = ExpenseHandler.handleDisplayExpenses("null", 999, "Groceries", 0, 0, 0);
        assertEquals(false, didWork);
    }

    /* Testing Display Expense with bad userid parameter */
    @Test
    public static void testDisplayExpenseBadUserid()
    {
        Boolean didWork = ExpenseHandler.handleDisplayExpenses("name", 0, "Groceries", 0, 0, 0);
        assertEquals(false, didWork);
    }

    /* Testing Display Expense with bad name parameter */
    @Test
    public static void testDisplayExpenseBadName()
    {
        Boolean didWork = ExpenseHandler.handleDisplayExpenses("name", 999, "", 0, 0, 0);
        assertEquals(false, didWork);
    }

    //Happy case where it should work to display expense by date
    @Test
    public static void displayExpenseDateHappyCase(){
        Boolean didWork = ExpenseHandler.handleDisplayExpenses("date", 605, "", 0, 0, 0);
        assertEquals(true, didWork);
    }

    //Happy case where it should display expenses by name
    @Test
    public static void displayExpenseNameHappyCase(){
        Boolean didWork = ExpenseHandler.handleDisplayExpenses("name", 605, "Entertainment", 0, 0, 0);
        assertEquals(true, didWork);
    }

    // Testing Display Expense with bad mode parameter
    @Test
    public void testFilterByPrice() {
        // Set up test data
        double minPrice = 10.0;
        double maxPrice = 100.0;
        int userId = 1;

        // Call the method to be tested
        Boolean didWork = ExpenseHandler.handleDisplayExpenses("price", userId, "", minPrice, maxPrice, 0);

        // Assert the expected result
        assertEquals(true, didWork); // Assuming the filter operation is successful
    }

    // Testing Display Expense with bad userid parameter
    @Test
    public void testFilterByCategory() {
        // Set up test data
        int categoryId = 3;
        int userId = 1;

        // Call the method to be tested
        Boolean didWork = ExpenseHandler.handleDisplayExpenses("category", userId, "", 0.0, 0.0, categoryId);

        // Assert the expected result
        assertEquals(true, didWork); // Assuming the filter operation is successful
    }

}
