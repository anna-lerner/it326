package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import Category.CategoryHandler;

public class CalculateTotalExpenseTest {


    @Test
    public void testDeleteCategory_Success() {

    
        CategoryHandler categoryHandler = new CategoryHandler();
        int categoryId = 123; // Existing category ID
        boolean result = categoryHandler.handleDeleteCategory(categoryId);
        assertTrue(result);

    }

    @Test
    public void testDeleteCategory_InvalidCategoryID() {
        CategoryHandler categoryHandler = new CategoryHandler();
        int categoryId = -1; //invalid category id
        boolean result = categoryHandler.handleDeleteCategory(categoryId);
        assertFalse(result);
    }

    @Test
    public void testDeleteCategory_NonExistentCategoryID() {
        CategoryHandler categoryHandler = new CategoryHandler();
        int categoryId = 999; // Non-existent category ID
        boolean result = categoryHandler.handleDeleteCategory(categoryId);
        assertFalse(result);
    }
    
}