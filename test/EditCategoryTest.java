package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import Category.CategoryHandler;

public class EditCategoryTest {


    @Test
    public void testEditCategory_Success() {
        // Arrange
        CategoryHandler categoryHandler = new CategoryHandler();
        int categoryId = 123; // Existing category ID
        String newName = "Utilities";
        double newBudget = 250.0;

        // Act
        boolean result = categoryHandler.handleEditCategory(categoryId, newName, newBudget);

        // Assert
        assertTrue(result);
        // Optionally, you can also verify if the category was edited successfully in the database
    }

    @Test
    public void testEditCategory_InvalidCategoryID() {
        // Arrange
        CategoryHandler categoryHandler = new CategoryHandler();
        int categoryId = -1; // Invalid negative category ID
        String newName = "Utilities";
        double newBudget = 250.0;

        // Act
        boolean result = categoryHandler.handleEditCategory(categoryId, newName, newBudget);

        // Assert
        assertFalse(result);
        // Optionally, you can verify if an appropriate error message is returned
    }

    @Test
    public void testEditCategory_EmptyName() {
        // Arrange
        CategoryHandler categoryHandler = new CategoryHandler();
        int categoryId = 123; // Existing category ID
        String newName = ""; // Empty name
        double newBudget = 250.0;

        // Act
        boolean result = categoryHandler.handleEditCategory(categoryId, newName, newBudget);

        // Assert
        assertFalse(result);
        // Optionally, you can verify if an appropriate error message is returned
    }

    @Test
    public void testEditCategory_NullName() {
        // Arrange
        CategoryHandler categoryHandler = new CategoryHandler();
        int categoryId = 123; // Existing category ID
        String newName = null; // Null name
        double newBudget = 250.0;

        // Act
        boolean result = categoryHandler.handleEditCategory(categoryId, newName, newBudget);

        // Assert
        assertFalse(result);
        // Optionally, you can verify if an appropriate error message is returned
    }
}
  