package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import User.User;
import User.UserController;

public class SendNotificationTest {
    
    public static void main(String[] args) {
        sendNotificationHappyCase();
        sendNotificationBadEmail();
    }

    //Happy case to send email notification
    @Test
    public static void sendNotificationHappyCase(){
        User user = new User(938, "grayson", "password", "grayson.newcomer@gmail.com", "GraysonNewcomer", 392384.34);
        boolean didWork = UserController.sendNotification(user, true, "email");
        assertEquals(didWork, true);
    }

    //Test case to send email with improper email that should not work.
    @Test
    public static void sendNotificationBadEmail(){
        User user = new User(938, "grayson", "password", "NotAnEmail", "GraysonNewcomer", 392384.34);
        boolean didWork = UserController.sendNotification(user, true, "email");
        assertEquals(didWork, false);
    }

}
