import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import Expense.Expense;
import Expense.ExpenseController;
import Expense.ExpenseHandler;
import Expense.ExpenseOperations;
import Category.Category;
import User.User;

public class ExpenseCreateTest {

    private ExpenseController controller;
    private ExpenseHandler handler;
    private StubExpenseOperations operations;  // Using a stub class instead of the real operations class

    @Before
    public void setUp() {
        operations = new StubExpenseOperations();
        handler = new ExpenseHandler(operations); // Assume ExpenseHandler needs ExpenseOperations
        controller = new ExpenseController(handler);
    }

    @Test
    public void testCreateExpenseSuccess() {
        Expense newExpense = new Expense(1, "Laptop", 1200.00, "2022-01-01", new Category(1, "Electronics", 5000), new User(1, "john_doe", "securepass123", "john@example.com", "John Doe", 60000));
        operations.setShouldCreateSucceed(true);  // Set the stub to succeed

        boolean result = controller.createExpense(newExpense);

        assertTrue("Expense should be created successfully", result);
    }

    @Test
    public void testCreateExpenseFailure() {
        Expense newExpense = new Expense(1, "", -200.00, "2022-01-01", new Category(1, "Electronics", 5000), new User(1, "john_doe", "securepass123", "john@example.com", "John Doe", 60000));
        operations.setShouldCreateSucceed(false);  // Set the stub to fail due to invalid data

        boolean result = controller.createExpense(newExpense);

        assertFalse("Expense should fail to be created due to invalid data", result);
    }

    // Stub class to simulate ExpenseOperations
    private static class StubExpenseOperations extends ExpenseOperations {
        private boolean shouldCreateSucceed;

        public void setShouldCreateSucceed(boolean shouldCreateSucceed) {
            this.shouldCreateSucceed = shouldCreateSucceed;
        }

        @Override
        public boolean createExpense(Expense expense) {
            // Simulate success or failure based on the stub setting
            return shouldCreateSucceed;
        }
    }
}
