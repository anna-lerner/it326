import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import Expense.ExpenseController;
import Expense.ExpenseHandler;
import Expense.ExpenseOperations;

public class ExpenseDeleteTest {

    private ExpenseController controller;
    private ExpenseHandler handler;
    private StubExpenseOperations operations;  // Using a stub class instead of the real operations class

    @Before
    public void setUp() {
        operations = new StubExpenseOperations();
        handler = new ExpenseHandler(operations); // Assume ExpenseHandler needs ExpenseOperations
        controller = new ExpenseController(handler);
    }

    @Test
    public void testDeleteExpenseSuccess() {
        int expenseID = 1;  // Assume an existing expense ID
        operations.setShouldDeleteSucceed(true);  // Set the stub to succeed

        boolean result = controller.deleteExpense(expenseID);

        assertTrue("Expense should be deleted successfully", result);
    }

    @Test
    public void testDeleteExpenseFailure() {
        int expenseID = -1;  // Assume a non-existing or invalid expense ID
        operations.setShouldDeleteSucceed(false);  // Set the stub to fail

        boolean result = controller.deleteExpense(expenseID);

        assertFalse("Expense should fail to be deleted", result);
    }

    // Stub class to simulate ExpenseOperations
    private static class StubExpenseOperations extends ExpenseOperations {
        private boolean shouldDeleteSucceed;

        public void setShouldDeleteSucceed(boolean shouldDeleteSucceed) {
            this.shouldDeleteSucceed = shouldDeleteSucceed;
        }

        @Override
        public boolean deleteExpense(int expenseID) {
            // Simulate success or failure based on the stub setting
            return shouldDeleteSucceed;
        }
    }
}
