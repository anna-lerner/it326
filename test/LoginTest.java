import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import User.User;
import User.UserController;
import User.UserHandler;
import User.UserOperations;

public class UserLoginTest {

    private UserController controller;
    private UserHandler handler;
    private StubUserOperations operations;  // Using a stub class instead of the real operations class

    @Before
    public void setUp() {
        operations = new StubUserOperations();
        handler = new UserHandler(operations); // Assume UserHandler needs UserOperations
        controller = new UserController(handler);
    }

    @Test
    public void testLoginSuccess() {
        String username = "john_doe";
        String password = "securepass123";
        operations.setShouldLoginSucceed(true);  // Set the stub to succeed

        boolean result = controller.loginUser(username, password);

        assertTrue("User should log in successfully", result);
    }

    @Test
    public void testLoginFailure() {
        String username = "john_doe";
        String password = "wrongpass";
        operations.setShouldLoginSucceed(false);  // Set the stub to fail

        boolean result = controller.loginUser(username, password);

        assertFalse("User should fail to log in with incorrect credentials", result);
    }

    // Stub class to simulate UserOperations
    private static class StubUserOperations extends UserOperations {
        private boolean shouldLoginSucceed;

        public void setShouldLoginSucceed(boolean shouldLoginSucceed) {
            this.shouldLoginSucceed = shouldLoginSucceed;
        }

        @Override
        public boolean checkCredentials(String username, String password) {
            // Simulate success or failure based on the stub setting
            return shouldLoginSucceed;
        }
    }
}
