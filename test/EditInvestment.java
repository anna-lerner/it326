import static org.junit.Assert.*;
import org.junit.Test;

public class EditInvestmentTest {

    @Test
    public void testEditInvestment() {
        // Edit an existing investment
        String username = "testUser";
        String password = "testPassword";
        int investmentID = 123; // ID of the investment to edit
        String newName = "Updated Investment";
        double newAmount = 1500.0;

        // Perform the edit operation
        boolean edited = InvestmentController.editInvestment(username, password, investmentID, newName, newAmount);

        // Assert that the investment was successfully edited
        assertTrue(edited);
    }
}
