import static org.junit.Assert.*;
import org.junit.Test;

public class GetStockPriceTest {

    @Test
    public void testGetStockPrice() {
        // Symbol of the stock to fetch the price for
        String symbol = "AAPL";

        // Fetch the stock price
        double stockPrice = InvestmentController.getStockPrice(symbol);

        // Ensure that the stock price is greater than zero
        assertTrue(stockPrice > 0);
    }
}
