// This class represents the User entity with basic attributes
package User;

public class User
{
    private int userid;
    private String username;
    private String password;
    private String email;
    private String name;
    private double income;

    public User(){

    }

    public User(int userid, String username, String password, String email, String name, double income)
    {   
        this.userid = userid;
        this.username = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.income = income;
    }

    //Getters
    public int getUserid(){
        return userid;
    }

    public String getEmail(){
        return email;
    }

    public String getName(){
        return name;
    }

    public double getIncome(){
        return income;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }

    //Setters
    public void setUserid(int userid){
        this.userid = userid;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setIncome(double income){
        this.income = income;
    }
}
