// This class contains the logic for user authentication
package User;

import Notification.EmailNotification;
import Notification.NotificationFactory;

public class UserHandler {
    private UserOperations userOperations;

    public UserHandler(UserOperations userOperations) {
        this.userOperations = userOperations;
    }

    // Create account use case
    public static Boolean handleCreateProfile(int userid, String username, String password, String email, String name,
            double income) {
        if (username != "" && password != "" && email != "" && name != "") {
            User user = new User(userid, username, password, email, name, income);
            UserOperations.createProfile(user);
            return true;
        } else {
            System.out.println("Not enough user information to create profile.");
            return false;
        }

    }

    // Send Notification use case
    public static boolean handleSendNotification(User user, Boolean isPerformanceGood, String mode) {
        if (user.getEmail() != "" && (mode == "email" || mode == "sms")) {
            NotificationFactory.getNotification(user, isPerformanceGood, mode, user.getEmail());
            EmailNotification en = new EmailNotification(user, isPerformanceGood, user.getEmail());
            return UserOperations.sendNotification(en);
        } else {
            return false;
        }
    }

    public static User handleLogout(User user) {
        if (user.getUserid() == 0 || user.getUsername() == null || user.getPassword() == null || user.getEmail() == null
                || user.getName() == null) {
            System.out.println("Invalid user.");
            return user;
        } else {
            return UserOperations.logout(user);
        }
    }

    public static boolean handleLogin(String username, String password) {
        return UserOperations.checkCredentials(username, password);
    }

    public boolean authenticateUser(String username, String password) {
        User user = userOperations.getUserByUsername(username);
        return user != null && user.getPassword().equals(password);
    }
}
