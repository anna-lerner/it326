// This class is responsible for user operations, interacts with database
package User;

import Database.*;
import Database.SQLDatabase;
import Notification.EmailNotification;

// this is not real logic, just a prototype of what may be here
public class UserOperations {

    // Create profile use case
    public static void createProfile(User user) {
        SQLDatabase db = new SQLDatabase();
        int userid = user.getUserid();
        String username = user.getUsername();
        String password = user.getPassword();
        String email = user.getEmail();
        String name = user.getName();
        String income = String.valueOf(user.getIncome());
        db.storeUser(userid, username, password, email, name, income);
    }

    public static boolean sendNotification(EmailNotification en) {
        return en.sendNotification();
    }

    public static User logout(User user) {
        user.setUserid(0);
        user.setUsername(null);
        user.setPassword(null);
        user.setEmail(null);
        user.setName(null);
        user.setIncome(0);
        return user;
    }

    // this will actually be a database interaction
    public User getUserByUsername(String username) {
        // mock user
        if (("admin").equals(username)) {
            // return new User("admin", "password");
        }
        return null;
    }

    public static boolean checkCredentials(String username, String password) {
        SQLDatabase db = (SQLDatabase) DatabaseFactory.createSQLDatabase();
        return db.verifyUserCredentials(username, password);
    }
}