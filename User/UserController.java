// This class acts as an interface between user input from the command line and the handler
package User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserController {

    public UserHandler userHandler;

    // Database connection details
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/it326";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "r0otp@s5word!";

    User user = new User();

    private static User currentUser = null;

    public static void setLoggedInUser(User user) {
        currentUser = user;
    }

    public static User getLoggedInUser() {
        return currentUser;
    }

    public UserController(UserHandler userHandler) {
        this.userHandler = userHandler;
    }

    // Send notification use case
    public static boolean sendNotification(User user, Boolean isPerformanceGood, String mode) {
        return UserHandler.handleSendNotification(user, isPerformanceGood, mode);
    }

    // Create account use case
    public static void createProfile(int userid, String username, String password, String email, String name,
            double income) {

        UserHandler.handleCreateProfile(userid, username, password, email, name, income);

    }

    // Logout use case
    public static User logout(User user) {
        return UserHandler.handleLogout(user);
    }

    public static boolean loginUser(String username, String password) {
        return UserHandler.handleLogin(username, password);
    }


    public static User loginUserAndGetInfo(String username, String password) {
        String sql = "SELECT userid, users, passwords, email, name, income FROM accounts WHERE users = ? AND passwords = ?";
        try (Connection conn = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                // Fetch user details from the result set
                int userId = rs.getInt("userid");
                String email = rs.getString("email");
                String name = rs.getString("name");
                double income = rs.getDouble("income");
                // Create a new User object with the fetched details
                User user = new User(userId, username, password, email, name, income);
                // Set the current User object to the newly created User object
                setLoggedInUser(user);
                return user;
            }
        } catch (SQLException e) {
            System.err.println("Database error during login: " + e.getMessage());
        }
        return null;
    }

    public static boolean authenticateUser(String username, String password) {
        try {
            // Database connection stuff
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);

            // Build query
            String selectQuery = "SELECT * FROM accounts WHERE users = ? AND passwords = ?";
            PreparedStatement preparedStatement = con.prepareStatement(selectQuery);

            // Setting parameter values
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);

            // Executing the SELECT statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Check if resultSet has any rows
            if (resultSet.next()) {
                // User found, authentication successful
                return true;
            } else {
                // User not found, authentication failed
                return false;
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return false; // Return false if an exception occurs (authentication failed)
        }
    }

}
