package Expense;

import User.User;
import Category.Category;

public class Expense {
    private int expenseID;
    private String name;
    private double amount;
    private String date;
    private Category category;
    private User user;

    public Expense(int expenseID, String name, double amount, String date, Category category, User user) {
        this.expenseID = expenseID;
        this.name = name;
        this.amount = amount;
        this.date = date;
        this.category = category;
        this.user = user;
    }

    // Getters and Setters
    public int getExpenseID() {
        return expenseID;
    }

    public void setExpenseID(int expenseID) {
        this.expenseID = expenseID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Category getCategory() {
        return category;
    }

    public User getUser() {
        return user;
    }
}
