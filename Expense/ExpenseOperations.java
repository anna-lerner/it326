package Expense;

import Database.DatabaseFactory;
import Database.SQLDatabase;

public class ExpenseOperations {
    private static SQLDatabase db = DatabaseFactory.createSQLDatabase();

    public static void createExpense(Expense expense) {
        db.storeExpense(expense); // Pass the entire Expense object
    }

    public static void deleteExpense(int expenseID) {
        db.deleteExpense(expenseID);
    }

    public static void editExpense(Expense expense) {
        db.updateExpense(expense); // Pass the entire Expense object
    }

    public static double calculateTotalExpenses(int userID) {
        return db.calculateTotalExpenses(userID);

    }

    public static void displayExpenses(String mode, int userId, String expenseName, double minPrice, double maxPrice,
            int categoryId) {
        db.retrieveExpenses(mode, userId, expenseName, minPrice, maxPrice, categoryId);
    }

    public static  void getAllExpenses(int userID)
    {
        db.getAllExpenses(userID);
        
    }
}
