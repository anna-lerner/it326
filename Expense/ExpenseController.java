package Expense;

public class ExpenseController {
    private ExpenseHandler expenseHandler;

    public ExpenseController() {
        this.expenseHandler = new ExpenseHandler();
    }

    public boolean createExpense(Expense expense) {
        return expenseHandler.handleCreateExpense(expense);
    }

    public boolean deleteExpense(int expenseID) {
        return expenseHandler.handleDeleteExpense(expenseID);
    }

    public boolean editExpense(Expense expense) {
        return expenseHandler.handleEditExpense(expense);
    }

    public static void displayExpenses(String mode, int userId, String expenseName, double minPrice, double maxPrice,
            int categoryId) {
        ExpenseHandler.handleDisplayExpenses(mode, userId, expenseName, minPrice, maxPrice, categoryId);
    }

    public double calculateTotalExpenses(int userID) {
        return ExpenseHandler.handleCalculatingTotalExpenses(userID);
    }
}

//     public static boolean getAllExpenses(int userID)
//     {
//          // return expenseHandler.handleAllExpenses(userID);
//     }
// }
