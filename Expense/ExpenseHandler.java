package Expense;

import Category.CategoryOperations;

public class ExpenseHandler {
    // This class handles the validation and processing logic for expenses.

    private ExpenseOperations expenseOperations;
    public ExpenseHandler()
    {
        this.expenseOperations = new ExpenseOperations();
    }

    public boolean handleCreateExpense(Expense expense) {
        // Validate expense before creation
        if (expense.getName() != null && !expense.getName().isEmpty() && expense.getAmount() > 0) {
            expenseOperations.createExpense(expense);
            return true;
        }
        return false;
    }

    public boolean handleDeleteExpense(int expenseID) {
        // Validate ID before deletion
        if (expenseID > 0) {
            ExpenseOperations.deleteExpense(expenseID);
            return true;
        }
        return false;
    }

    public  boolean handleEditExpense(Expense expense) {
        // Validate expense before editing
        if (expense.getExpenseID() > 0 && expense.getName() != null && !expense.getName().isEmpty()
                && expense.getAmount() > 0) {
            ExpenseOperations.editExpense(expense);
            return true;
        }
        return false;
    }

    public static double handleCalculatingTotalExpenses(int userID) {

        if (userID != 0) {
            return ExpenseOperations.calculateTotalExpenses(userID);
        }
        return 0;
    }

    // Validate display expense information
    public static Boolean handleDisplayExpenses(String mode, int userid, String expenseName, double minPrice, double maxPrice,
            int categoryID) {
        if (mode.equals("date") || mode.equals("name") || userid != 0) {
            ExpenseOperations.displayExpenses(mode, userid, expenseName, minPrice, maxPrice, categoryID);
            return true;
        } else {
            System.out.println("Information invalid.");
            return false;
        }
    }


    public static boolean handleAllExpenses(int userID)
    {
        if(userID <= 0)
        {
            return false;
        }

        else{
            ExpenseOperations.getAllExpenses(userID);
            return true;

        }
    }
}

