// import java.util.Date;

// public class DataTrend {
//     private Date dateCreated;
//     private Portfolio inputData;

//     // Constructor
//     public DataTrend(Date dateCreated, Portfolio inputData) {
//         this.dateCreated = dateCreated;
//         this.inputData = inputData;
//     }

//     // Method to export data trend
//     public boolean exportDataTrend() {
//         // Check if inputData contains valid data (expenses or investments)
//         if (inputData != null && (inputData.getExpenses().size() > 0 || inputData.getInvestments().size() > 0)) {
//             // Generate data trend based on inputData
//             // Export the data trend (e.g., to a file or display it)
//             System.out.println("Data trend exported successfully.");
//             return true;
//         } else {
//             // If no valid data found, display an error message
//             System.out.println("Error: No expenses or investments found in the portfolio.");
//             return false;
//         }
//     }

//     // Getters and setters
//     public Date getDateCreated() {
//         return dateCreated;
//     }

//     public void setDateCreated(Date dateCreated) {
//         this.dateCreated = dateCreated;
//     }

//     public Portfolio getInputData() {
//         return inputData;
//     }

//     public void setInputData(Portfolio inputData) {
//         this.inputData = inputData;
//     }
// }
