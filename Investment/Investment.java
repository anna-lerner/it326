package Investment;
public class Investment {
    private int investmentID;
    private String name;
    private double investmentAmount;

    public Investment(double investmentID, String name, double investmentAmount) {
        this.investmentID = (int) investmentID;
        this.name = name;
        this.investmentAmount = investmentAmount;
    }

    // Getters and Setters
    public int getInvestmentID() {
        return investmentID;
    }

    public void setInvestmentID(int investmentID) {
        this.investmentID = investmentID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(double investmentAmount) {
        this.investmentAmount = investmentAmount;
    }
}
