package Investment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class InvestmentOperations {

    // Database connection details
    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/it326";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "r0otp@s5word!";

    // Method to create an investment in the database
    public int createInvestment(Investment investment) {
        String sql = "INSERT INTO investments (name, investmentAmount) VALUES (?, ?)";
        try (Connection conn = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            pstmt.setString(1, investment.getName());
            pstmt.setDouble(2, investment.getInvestmentAmount());
            pstmt.executeUpdate();

            // Retrieve the auto-generated investment ID
            ResultSet generatedKeys = pstmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1; // Return -1 if insertion fails
    }

    // Method to delete an investment from the database
    public void deleteInvestment(int investmentID) {
        String sql = "DELETE FROM investments WHERE investmentID = ?";
        try (Connection conn = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, investmentID);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Method to edit an existing investment in the database
    public void editInvestment(Investment investment) {
        String sql = "EDIT investments SET name = ?, investmentAmount = ? WHERE investmentID = ?";
        try (Connection conn = DriverManager.getConnection(JDBC_URL, USERNAME, PASSWORD);
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, investment.getName());
            pstmt.setDouble(2, investment.getInvestmentAmount());
            pstmt.setInt(3, investment.getInvestmentID());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
