package Investment;

public class InvestmentController {

    private static InvestmentHandler handler;

    static {
        handler = new InvestmentHandler();
    }

    public static int createInvestment(String username, String password, String name, double investmentAmount) {
        if (handler.authenticateUser(username, password)) {
            InvestmentOperations operations = new InvestmentOperations();
            Investment investment = new Investment(Math.random() * 1000, name, investmentAmount);
            int investmentID = operations.createInvestment(investment);
            if (investmentID != -1) {
                System.out.println("Investment created successfully. Investment ID: " + investmentID);
            } else {
                System.out.println("Failed to create investment.");
            }
            return investmentID;
        } else {
            System.out.println("Authentication failed. Please check your credentials.");
            return -1;
        }
    }

    public static void editInvestment(int investmentID, String name, double investmentAmount) {
        handler.handleEditInvestment(investmentID, name, investmentAmount);
    }

    public static void deleteInvestment(int investmentID) {
        handler.handleDeleteInvestment(investmentID);
    }
}
