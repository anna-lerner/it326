package Investment;

import org.json.JSONObject;
import User.UserController;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class InvestmentHandler {

    private static final String API_BASE_URL = "https://api.twelvedata.com/price";
    private static final String API_KEY = "87d72f32f9e34d668e1a625cd1bb23a6";
    private static final HttpClient client = HttpClient.newHttpClient();

    public boolean authenticateUser(String username, String password) {
        // Use UserController to authenticate user
        return UserController.authenticateUser(username, password);
    }

    public boolean validateInvestmentData(String name, double investmentAmount) {
        // Logic to validate investment data
        return name != null && !name.isEmpty() && investmentAmount > 0;
    }

    public void handleCreateInvestment(String name, double investmentAmount) {
        if (validateInvestmentData(name, investmentAmount)) {
            Investment investment = new Investment(generateInvestmentID(), name, investmentAmount);
            InvestmentOperations operations = new InvestmentOperations();
            operations.createInvestment(investment);
        } else {
            System.out.println("Invalid investment data. Please provide valid name and investment amount.");
        }
    }

    public void handleEditInvestment(int investmentID, String name, double investmentAmount) {
        if (validateInvestmentData(name, investmentAmount)) {
            Investment investment = new Investment(investmentID, name, investmentAmount);
            InvestmentOperations operations = new InvestmentOperations();
            operations.editInvestment(investment);
        } else {
            System.out.println("Invalid investment data. Please provide valid name and investment amount.");
        }
    }

    public void handleDeleteInvestment(int investmentID) {
        // Logic to handle deletion of investment
        InvestmentOperations operations = new InvestmentOperations();
        operations.deleteInvestment(investmentID);
    }

    private int generateInvestmentID() {
        // Logic to generate a unique investment ID
        return (int) (Math.random() * 1000);
    }

    public static double getStockPrice(String symbol) {
        try {
            String url = API_BASE_URL + "?symbol=" + symbol + "&apikey=" + API_KEY;

            HttpRequest request = HttpRequest.newBuilder(new URI(url))
                    .GET()
                    .build();

            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            // Check if request was successful (status code 200)
            if (response.statusCode() == 200) {
                return parseStockPrice(response.body());
            } else {
                System.out.println("Failed to fetch data from API: " + response.statusCode());
                return -1;
            }
        } catch (URISyntaxException | IOException | InterruptedException e) {
            System.out.println("Error fetching stock price: " + e.getMessage());
            return -1;
        }
    }

    private static double parseStockPrice(String responseBody) {
        try {
            JSONObject jsonResponse = new JSONObject(responseBody);

            // Check if the response contains the "price" field
            if (jsonResponse.has("price")) {
                return jsonResponse.getDouble("price");
            } else {
                System.out.println("No price found in JSON response");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Error parsing JSON response: " + e.getMessage());
            return -1;
        }
    }
}
